import cv2
import numpy as np
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
from skimage.color import gray2rgb


def find_image(im, tpl):
    im = np.atleast_3d(im)
    tpl = np.atleast_3d(tpl)
    H, W, D = im.shape[:3]
    h, w = tpl.shape[:2]

    sat = im.cumsum(1).cumsum(0)
    tplsum = np.array([tpl[:, :, i].sum() for i in range(D)])

    # находим возможные совпадения по сумме
    iA, iB, iC, iD = sat[:-h, :-w], sat[:-h, w:], sat[h:, :-w], sat[h:, w:]
    lookup = iD - iB - iC + iA #https://robocraft.ru/computervision/536

    possible_match = np.where(np.logical_and.reduce([lookup[..., i] == tplsum[i] for i in range(D)]))
    # return possible_match
    # проверка точного соответствия
    for y, x in zip(*possible_match):
        if np.all(im[y+1:y+h+1, x+1:x+w+1] == tpl):
            return (y+1, x+1)

    raise Exception("Image not found")

mask = cv2.imread('imgs/7_1.jpg', 0)
im = gray2rgb(mask)
tpl = im[400:500, 680:900].copy() #берем значения исключительно с картинки, ииначе работать не будет
plt.imshow(tpl)
y, x = find_image(im, tpl)
fig, ax = plt.subplots()
plt.imshow(im)

rect = Rectangle((x, y), tpl.shape[1], tpl.shape[0], edgecolor='r', facecolor='none')
ax.add_patch(rect)
plt.show()


#проверка возможных найденных окон
# arr_possible = find_image(im, tpl)
# print(arr_possible)
# for y, x in zip(*arr_possible):
#     fig, ax = plt.subplots()
#     plt.imshow(im)
#
#     rect = Rectangle((x, y), tpl.shape[1], tpl.shape[0], edgecolor='r', facecolor='none')
#     ax.add_patch(rect)
#     plt.show()

#координаты для фото
# arr = [[732:946, 231:448], 1_1
#        [839:939, 225:425], 2_1
#        [850:980, 630:750], 4-1
#        [448:500, 341:381], 4_1
#        [588:678, 640:780], 3_1
#        [448:624, 636:743], 5_1
#        [739:777, 417:470], 4_2
#        [900:1000, 587:700], 4_2
#        [690:815, 480:560], 6_1
#        [], 7_1
#        ]
