"""Поиск ключевых точек эталона на входном изображении (ORB)"""

import numpy as np
import cv2
from matplotlib import pyplot as plt

query_img = cv2.imread('imgs/7_1.jpg')
#original_img = cv2.imread('imgs/5_5.jpg')
original_img = query_img[400:500, 680:900].copy()
query_img_bw = cv2.cvtColor(query_img, cv2.IMREAD_GRAYSCALE)
original_img_bw = cv2.cvtColor(original_img, cv2.IMREAD_GRAYSCALE)

orb = cv2.ORB_create()
queryKP, queryDes = orb.detectAndCompute(query_img_bw,None)
trainKP, trainDes = orb.detectAndCompute(original_img_bw,None)

matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
matches = matcher.match(queryDes,trainDes)
matches = sorted(matches, key = lambda x:x.distance)

#чтобы нарисовать прямоугольник
src_pts = np.float32([queryKP[m.queryIdx].pt for m in matches[:40]]).reshape(-1,1,2)
dst_pts = np.float32([trainKP[m.trainIdx].pt for m in matches[:40]]).reshape(-1,1,2)
M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
matchesMask = mask.ravel().tolist()
pts = src_pts[mask==1]
min_x, min_y = np.int32(pts.min(axis=0))
max_x, max_y = np.int32(pts.max(axis=0))

a = cv2.rectangle(query_img,(min_x, min_y), (max_x,max_y), 255,2)
final_img = cv2.drawMatches(a, queryKP,
                            original_img, trainKP, matches[:40],None)


cv2.imshow("Result", final_img)
cv2.waitKey()

isWritten = cv2.imwrite('src/image-10.png', final_img)

if isWritten:
	print('Image is successfully saved as file.')