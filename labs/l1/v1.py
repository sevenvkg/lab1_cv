"""Бинаризация с адаптивным порогом. На вход поступает изображение,
программа отрисовывает окно, в которое выводится либо исходное
изображение после преобразования в черно-белое, либо после
бинаризации (переключение по нажатию клавиши)."""

import cv2 as cv
from matplotlib import pyplot as plt
import numpy as np
from scipy import ndimage
from PIL import Image
import copy
import time


def adaptive_threshold_cv(img):

    img = cv.medianBlur(img,5)
    ret,th1 = cv.threshold(img,127,255,cv.THRESH_BINARY)
    th2 = cv.adaptiveThreshold(img,255,cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY,11,2)
    th3 = cv.adaptiveThreshold(img,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,11,2)
    titles = ['Original Image', 'Global Thresholding (v = 127)',
                'Adaptive Mean Thresholding', 'Adaptive Gaussian Thresholding']
    images = [img, th1, th2, th3]
    for i in range(4):
        plt.subplot(2,2,i+1),plt.imshow(images[i],'gray')
        plt.title(titles[i])
        plt.xticks([]),plt.yticks([])
    plt.show()
    return th3


def bradley_threshold(image, threshold=75, windowsize=5):

    ws = windowsize
    image2 = copy.copy(image).convert('L')
    w, h = image.size
    l = image.convert('L').load()
    l2 = image2.load()
    threshold /= 100.0
    for y in range(h):
        for x in range(w):
            #поиск соседних пикселей
            neighbors =[(x+x2,y+y2) for x2 in range(-ws,ws) for y2 in range(-ws, ws) if x+x2>0 and x+x2<w and y+y2>0 and y+y2<h]
            #значение всех соседних пикселей
            mean = sum([l[a,b] for a,b in neighbors])/len(neighbors)
            if l[x, y] < threshold*mean:
                l2[x,y] = 0
            else:
                l2[x,y] = 255
    return image2

def faster_bradley_threshold(image, threshold=75, window_r=5):

    percentage = threshold / 100.
    window_diam = 2*window_r + 1
    # convert image to numpy array of grayscale values
    img = np.array(image.convert('L')).astype(np.float)
    # матрица локальных значений
    means = ndimage.uniform_filter(img, window_diam)
    #  0 если меньше percentage*mean, 255 если больше
    height, width = img.shape[:2]
    result = np.zeros((height,width), np.uint8)
    #Если значение img больше или равно произведению, тогда все значения,
    # которые удовлетворяют этому логическому выражению заменяются на 255
    result[img >= percentage * means] = 255

    return Image.fromarray(result)

def show_result(img_before, img_after):
    images = [img_before, img_after]
    titles = ["before", "after"]
    for i in range(2):
        plt.subplot(1,2,i+1),plt.imshow(images[i],'gray')
        plt.title(titles[i])
        plt.xticks([]),plt.yticks([])
    plt.show()

if __name__ == '__main__':

    t0 = time.process_time()
    img_cv = cv.imread('imgs/5.jpg', 0)
    thr = adaptive_threshold_cv(img_cv)
    print('Длительность обработки с помощью OpenCV', round(time.process_time()-t0, 3), 's')

    img_br = Image.open('imgs/5.jpg')
    t0 = time.process_time()
    threshed0 = bradley_threshold(img_br)
    print('Длительность обработки с помощью массивов', round(time.process_time()-t0, 3), 's')
    show_result(img_br, threshed0)

    t0 = time.process_time()
    threshed1 = faster_bradley_threshold(img_br)
    print('Длительность обработки с помощью numpy & scipy :', round(time.process_time()-t0, 3), 's')
    show_result(img_br, threshed1)




